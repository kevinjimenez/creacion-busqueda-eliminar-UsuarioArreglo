const funciones = require('./paqueteDeFuncionesPromises')
const { Observable, Subject, ReplaySubject, from, of, range, throwError } = require('rxjs');
const { switchMap, map, filter, distinct, mergeMap, catchError } = require('rxjs/operators');

module.exports = (arregloUsuarios, parametroDeBusqueda) => {
    
    respuestaDelObservable = (respuesta) => {
        console.log(respuesta)
    }
    ErrorDelObservable = (error) => {
        console.log(error)
    }
    terminoElObservable = () => {
        console.log('termino')
    }

    const arregloYParametroDeBusqueda = {
        arregloUsuarios,
        parametroDeBusqueda
    }

    const observableDeBuscarDiferentesUsuarios$ = of(arregloYParametroDeBusqueda)

    const observableBuscarDiferentesUsuarios = (arregloYParametro) => {
        const promesaDeBuscarDiferentesUsuarios = from(funciones.buscarDiferentes(arregloYParametro.arregloUsuarios, arregloYParametro.parametroDeBusqueda))
        return promesaDeBuscarDiferentesUsuarios
    }

    observableDeBuscarDiferentesUsuarios$
        .pipe(
            mergeMap(observableBuscarDiferentesUsuarios)
        )
        .subscribe(
            respuestaDelObservable,
            ErrorDelObservable,
            terminoElObservable
        )
}
