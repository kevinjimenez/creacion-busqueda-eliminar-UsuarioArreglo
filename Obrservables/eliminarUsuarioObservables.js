const funciones = require('./paqueteDeFuncionesPromises.js')
const { Observable, Subject, ReplaySubject, from, of, range, throwError } = require('rxjs');
const { switchMap, map, filter, distinct, mergeMap, catchError } = require('rxjs/operators');

module.exports = (arregloUsuarios, usuarioACrear) => {
    

    respuestaDelObservable = (respuesta) => {
        console.log(respuesta)
    }
    ErrorDelObservable = (error) => {
        console.log(error)
    }
    terminoElObservable = () => {
        console.log('termino')
    }

    const arregloYUsuarioACrear = {
        arregloUsuarios,
        usuarioACrear
    }

    const observableDeEliminarUsuario$ = of(arregloYUsuarioACrear)

    const observableEliminarUsuario = (arregloYUsuario) => {
        const promesaDeEliminarUsuario = from(funciones.eliminarUsuario(arregloYUsuario.arregloUsuarios, arregloYUsuario.usuarioACrear))
        return promesaDeEliminarUsuario
    }

    observableDeEliminarUsuario$
        .pipe(
            mergeMap(observableEliminarUsuario)
        )
        .subscribe(
            respuestaDelObservable,
            ErrorDelObservable,
            terminoElObservable
        )
}
