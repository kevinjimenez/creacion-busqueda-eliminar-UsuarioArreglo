const funciones = require('./paqueteDeFuncionesPromises')
const { Observable, Subject, ReplaySubject, from, of, range, throwError } = require('rxjs');
const { switchMap, map, filter, distinct, mergeMap, catchError } = require('rxjs/operators');

module.exports = (arregloUsuarios, usuarioACrear) => {
    
    respuestaDelObservable = (respuesta) => {
        console.log(respuesta)
    }
    ErrorDelObservable = (error) => {
        console.log(error)
    }
    terminoElObservable = () => {
        console.log('termino')
    }

    const arregloYUsuarioACrear = {
        arregloUsuarios,
        usuarioACrear
    }

    const observableDeBuscarUsuario$ = of(arregloYUsuarioACrear)

    const observableBuscarUsuario = (arregloYUsuario) => {
        const promesaDeBuscarUsuario = from(funciones.buscarUsuario(arregloYUsuario.arregloUsuarios, arregloYUsuario.usuarioACrear))
        return promesaDeBuscarUsuario
    }

    observableDeBuscarUsuario$
        .pipe(
            mergeMap(observableBuscarUsuario)
        )
        .subscribe(
            respuestaDelObservable,
            ErrorDelObservable,
            terminoElObservable
        )
}
