const buscarUsuario = require('./Promises/buscarUsuario')
const crearUsuario = require('./Promises/crearUsuario.js')
const eliminarUsuario = require('./Promises/eliminarUsuario.js')
const buscarDiferentes = require('./Promises/buscarDiferentesUsuarios.js')
const datosDePrueba = require('./datosDePrueba.js')
//const buscarCrearUsuario = require('./buscarYCrearUsuario.js')
//const buscarDiferentesUsuarios = require('./buscarDiferentesUsuarios.js')

module.exports = {    
    datosDePrueba,
    crearUsuario,
    buscarUsuario,
    buscarDiferentes,
    eliminarUsuario,
    //buscarDiferentesUsuarios,
    //buscarCrearUsuario
}

