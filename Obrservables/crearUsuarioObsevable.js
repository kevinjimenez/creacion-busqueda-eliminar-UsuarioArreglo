const funciones = require('./paqueteDeFuncionesPromises')
const { Observable, Subject, ReplaySubject, from, of, range, throwError } = require('rxjs');
const { switchMap, map, filter, distinct, mergeMap, catchError } = require('rxjs/operators');

module.exports = (arregloUsuarios, usuarioACrear) => {
    console.log(usuarioACrear)

    respuestaDelObservable = (respuesta) => {
        console.log(respuesta)
    }
    ErrorDelObservable = (error) => {
        console.log(error)
    }
    terminoElObservable = () => {
        console.log('termino')
    }

    const arregloYUsuarioACrear = {
        arregloUsuarios,
        usuarioACrear
    }

    const observableDeCrearUsuario$ = of(arregloYUsuarioACrear)

    const observableCrearUsuario = (arregloYUsuario) => {
        const promesaDeCrearUsuario = from(funciones.crearUsuario(arregloYUsuario.arregloUsuarios, arregloYUsuario.usuarioACrear))
        return promesaDeCrearUsuario
    }

    observableDeCrearUsuario$
        .pipe(
            mergeMap(observableCrearUsuario)
        )
        .subscribe(
            respuestaDelObservable,
            ErrorDelObservable,
            terminoElObservable
        )
}
