const paquetes = require('./paquetes.js')

module.exports = (arreglo,usuarioAbuscarOCrear)=>{    
    const promiseDeBuscarYCrear = (resolve,reject)=>{
        paquetes.buscarUsuario(arreglo,usuarioAbuscarOCrear)
        .then(respuestaResolvePromiseBuscarUsuario=>{
            resolve({
                mensaje: respuestaResolvePromiseBuscarUsuario.mensaje,
                usuarioEncontrado: respuestaResolvePromiseBuscarUsuario.usuarioEncontrado
            })
            
        })
        .catch(respuestaRejectPromiseBuscarUsuario=>{    
            return paquetes.crearUsuario(arreglo,usuarioAbuscarOCrear)
        })
        .then(respuestaResolvePromiseCrearUsuario=>{
            resolve({
                mensaje: respuestaResolvePromiseCrearUsuario.mensaje,
                usuarios: respuestaResolvePromiseCrearUsuario.usuarios
            })
        }).catch(respuestaRejectPromiseCrearUsuario=>{
            reject({
                mensaje: respuestaRejectPromiseCrearUsuario,                
            })
        })

        

    }
    return new Promise(promiseDeBuscarYCrear)
    
}