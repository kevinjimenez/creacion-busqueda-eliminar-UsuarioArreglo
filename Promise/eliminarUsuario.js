
module.exports = (arreglo,usuarioAEliminar)=>{   
    let existeElUsuario = (usuario => (usuario.nombre === usuarioAEliminar.nombre && usuario.apellido === usuarioAEliminar.apellido))
    const indiceDelElementoABorrar = arreglo.findIndex(existeElUsuario)
    let existeElIndice = indiceDelElementoABorrar > -1
    
    const funcionPromiseDeEliminarUsuario = (resolve,reject)=>{
        if (existeElIndice) {            
            arreglo.splice(indiceDelElementoABorrar,1)
            resolve({
                mensaje: 'se elimino con exito',
                arreglo
            })            
        } else {
            reject({
                mensaje: 'no se pudo eliminar',            
            })    
        }
    }        
    return new Promise(funcionPromiseDeEliminarUsuario)        
}



